package com.example.m.spritetest;

import android.graphics.Bitmap;

public class Mover {

    // Declare an object of type Bitmap
    Bitmap bitmapBob;

    // Bob starts off not moving
    boolean isMoving = false;

    // He starts 10 pixels from the left
    float bobXPosition = 10;
}
